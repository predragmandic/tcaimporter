
tcaimporter
==================================================

Tool for importing data from json to example HTTP service. This tool is made only for the purpose of technical assignment and has no real use otherwise.
Why is it named tcaimporter? TCA = Technical Consultant Assignment :D

Requirements
-------------------------

- python >= 3.7.3
- wheel

Setup
-------------------------

Use pip to install (from project root):

```bash
pip3 install .
```

How to use
-------------------------

Use `tcaimporter [COMMAND] --help` to see general help or help for a specific command.

### daemon

Run in daemon mode to continuously look for new files in source directory and parse them. Make sure to have unique name for each daemon if you want to run multiple instances at the same time.

Example:

```bash
tcaimporter daemon -e https://httpbin.org/anything -s /ftp/incoming -n first -u username -p password -w 10
```

This will search for files in `/ftp/incoming` and make requests to `https://httpbin.org/anything` endpoint. It will use username `username` and password `password`. It will use 10 workers (-w) to send 10 requests at the same time.

**Important**: Make sure you use unique value for name (-n, --name) of the daemon instance since it will create operating directory with the same name. Unique instance names will avoid multiple instances interfering with each other. You can use this to setup systemd service with multiple instances of the same service. Take a look at https://www.stevenrombauts.be/2019/01/run-multiple-instances-of-the-same-systemd-unit/ for examples.

### single file

You can process single file manually when needed like this:

```bash
tcaimporter single -e https://httpbin.org/anything -u username -p password input.json
```

In case you want to continue processing from specific ID (e.g. service crashed and you don't want to process huge file from start), you can specify to begin with specific ID like this:

```bash
tcaimporter single -b 2142ee75-9ddf-4684-a237-fa698aaed60b -e https://httpbin.org/anything input.json
```

Limitations
-------------------------

### Filesystem

Since importer will move files before starting to process them, it is important that the destination is under same filesystem. Move operation is atomic in same filesystem, but if file is moved to another mountpoint it will be copied first and source deleted afterwards. It is possible that 2 instances of importer do this at the same time and that they will process a copy of the same file twice.

Logging
-------------------------

Service will log file names and id's of processed entries in the STDOUT. You can enable debug logging with -d or --debug option right after executable, followed by command and options:

```bash
tcaimporter --debug daemon -e https://httpbin.org/anything -s /ftp/incoming -n first -u username -p password
```

It is expected that 3rd party service is used to store these logs into log files (e.g. docker, systemd, etc).

