'''
Parsing and manipulating json files.
'''
import ijson
import logging
import os
import time

work_dirs = {
        "processing": "processing",
        "done": "done",
        "failed": "failed",
        }

def parse(json_file):
    """
    Parse top level of json object.
    """
    key = '-'
    for prefix, event, value in ijson.parse(json_file):
        if prefix == '' and event == 'map_key':
            key = value
            builder = ijson.common.ObjectBuilder()
        elif prefix.startswith(key):
            builder.event(event, value)
            if event == 'end_map':
                yield key, builder.value


class Processor():
    
    def __init__(self, source, name):
        main_logger = logging.getLogger('main')
        self.logger = logging.getLogger('parser')
        self.logger.setLevel(main_logger.level)
        self.source = source
        self.name = name
        for d in work_dirs.values():
            path = os.path.join(self.source, self.name, d)
            if not os.path.exists(path):
                os.makedirs(path)

    def next_input_file(self):
        """
        Finds first available file in source directory, moves it to processing directory and returns new path.
        If no files are available, it will retry every second until new files are available.
        If file failed to move, it will retry right away.
        If there are leftover files from previous session in processing directory, those will be returned instead.
        """
        processing_dir = os.path.join(self.source, self.name, work_dirs["processing"])
        for f in os.listdir(processing_dir):
            leftover_file = os.path.join(processing_dir, f)
            if os.path.isfile(leftover_file):
                self.logger.info(f'Processing leftover file {leftover_file}')
                return leftover_file

        while True:
            for f in os.listdir(self.source):
                src_file = os.path.join(self.source, f)
                if os.path.isfile(src_file):
                    dest_file = os.path.join(self.source, self.name, "processing", f)
                    try:
                        os.rename(src_file, dest_file)
                        self.logger.info(f'Processing file {f}')
                        return dest_file
                    except FileNotFoundError:
                        pass
                    break
            self.logger.debug(f'Looking for new files...')
            time.sleep(1)
    
    def store_processed_file(self):
        '''Move any file(s) found in processing directory to done directory.'''
        processing_dir = os.path.join(self.source, self.name, work_dirs["processing"])
        done_dir = os.path.join(self.source, self.name, work_dirs["done"])
        for f in os.listdir(processing_dir):
            if os.path.isfile(os.path.join(processing_dir, f)):
                self.logger.debug(f'Moving processed file {f}')
                os.rename(os.path.join(processing_dir, f), os.path.join(done_dir, f))

    def store_failed_file(self):
        '''Move any file(s) found in processing directory to failed directory.'''
        processing_dir = os.path.join(self.source, self.name, work_dirs["processing"])
        failed_dir = os.path.join(self.source, self.name, work_dirs["failed"])
        for f in os.listdir(processing_dir):
            if os.path.isfile(os.path.join(processing_dir, f)):
                self.logger.debug(f'Moving failed file {f}')
                os.rename(os.path.join(processing_dir, f), os.path.join(failed_dir, f))

    def next_entry(self):
        '''Yield a dict containing next entry from picked up file.
        Will switch to next available file when done parsing.
        Will move files to done directory upon finished parsing or to failed if any error occured.
        '''
        while True:
            file_path = self.next_input_file()
            with open(file_path, "rb") as json_file:
                try:
                    for key, value in parse(json_file):
                        yield key, value
                except:
                    self.logger.error(f'Failed to process {file_path}')
                    self.store_failed_file()
            self.store_processed_file()

