"""
tcaimporter - tech consultant assignment
"""
__version__ = "0.0.1"
__license__ = "BSD"
__year__ = "2021"
__author__ = "Predrag Mandic"
__author_email__ = "predrag@nul.one"
__copyright__ = f"Copyright {__year__} {__author__} <{__author_email__}>"

class TcaimporterError(Exception):
    "Generic exception."
    pass

