'''
Formatting data and making requests to endpoint.
'''
import hashlib
import logging
import requests
import time


class Importer:
    '''Class for making requests to the endpoint.'''
    def __init__(self, endpoint, auth, requests_per_second=10):
        main_logger = logging.getLogger('main')
        self.logger = logging.getLogger('importer')
        self.logger.setLevel(main_logger.level)
        self.requests_per_second = requests_per_second
        self.endpoint = endpoint
        self.auth = auth
        self.count = 0
        self.seconds = time.time()

    def mark_count(self):
        '''Keep track of number of requests.
        Will sleep if max requests are reached.
        '''
        now_seconds = time.time()
        if now_seconds - self.seconds > 1:
            self.seconds = now_seconds
            self.count = 1
            return
        if self.count >= self.requests_per_second:
            self.logger.debug("Waiting to avoid passing max requests.")
            time.sleep(1 - (now_seconds - self.seconds))
            self.seconds = time.time()
            self.count = 1
            return
        self.count += 1
        return
    
    def make_request(self, url, data):
        '''Make request to the endpoint.
        Will retry indefinitely if status code is not 200.
        '''
        while True:
            self.mark_count()
            self.logger.debug(f'Making request to {url} with data:\n{data}')
            resp = requests.post(url, data=data, auth=self.auth)
            self.logger.debug(f'Got response status code {resp.status_code} with content:\n{resp.content}')
            if resp.status_code == 429:
                self.logger.debug("Max requests reached. Waiting full second.")
                time.sleep(1)
                continue
            if resp.status_code != 200:
                self.logger.warn(f"Status code was {resp.status_code}. Will retry...")
                continue
            break

    def import_entry(self, entry):
        '''Hash entry data and make a request to the endpoint.
        In case the data is malformed it will only log and skip.
        '''
        self.mark_count()
        try:
            data = {
                "factName": "crm_data",
                "factTtl": 63072000, #TODO: should we parametrize this?
                "properties": {
                    "email": hashlib.sha256(bytes(entry[1]['email'], 'utf-8')).hexdigest(),
                    "phone_number": hashlib.sha256(bytes(str(entry[1]['phone_number']), 'utf-8')).hexdigest(),
                    "creationTime": str(entry[1]['creattion_time']),
                    "location": str(entry[1]['location'])
                    }
                }
            url = f"{self.endpoint}?partnerId={entry[0]}&forceInsert=false" # should forceInsert be parametrized?
        except Exception as e:
            self.logger.error("Failed to format the data. Entry may contain malformed data.")
            return
        self.logger.info(f"Importing {entry[0]}")
        self.make_request(url, data)
        
