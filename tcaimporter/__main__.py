"""
tcaimporter main module
"""
from tcaimporter import processor, importer
import click
import logging
import os
import queue
import tcaimporter as app
import threading

logging.basicConfig()
logger = logging.getLogger('main')
logger.setLevel(logging.INFO)

@click.group()
@click.version_option(version=app.__version__,
    message=f"%(prog)s %(version)s - {app.__copyright__}")
@click.option('-d', '--debug', is_flag=True,
    help="Enable debug mode with output of each action in the log.")
@click.pass_context
def cli(ctx, **kwargs):
    if ctx.params.get('debug'):
        logger.setLevel(logging.DEBUG)
        logger.info("Starting in debug mode.")

@cli.command()
@click.option('-n', '--name', type=str, required=True,
    help="Unique name of daemon instance.")
@click.option('-s', '--source', type=click.Path(exists=True, readable=True), required=True,
    help="Path to directory containing json files.")
@click.option('-e', '--endpoint', type=str, required=False, default="https://tc-assignment.r42-apps.com/entries",
    help="Endpoint for HTTP request.")
@click.option('-u', '--username', type=str, required=False, default=None,
    help="Username for basic auth.")
@click.option('-p', '--password', type=str, required=False, default=None,
    help="Password for basic auth.")
@click.option('-w', '--workers', type=int, required=False, default=1,
    help="Number of parallel workers. Defaults to 1.")
def daemon(**kwargs):
    "Run the importer service."
    p = processor.Processor(name=kwargs['name'], source=kwargs['source'])
    i = importer.Importer(endpoint=kwargs['endpoint'],
            auth=(kwargs['username'], kwargs['password'])
            )
    logger.info(f'Started daemon "{kwargs["name"]}"')
    q = queue.Queue(maxsize=kwargs['workers']*2)
    def worker():
        while True:
            i.import_entry(q.get())
    for n_workers in range(0, kwargs['workers']):
        threading.Thread(target=worker, daemon=True).start()
    for entry in p.next_entry():
        q.put(entry)

@cli.command()
@click.option('-e', '--endpoint', type=str, required=False, default="https://tc-assignment.r42-apps.com/entries",
    help="Endpoint for HTTP request.")
@click.option('-u', '--username', type=str, required=False, default=None,
    help="Username for basic auth.")
@click.option('-p', '--password', type=str, required=False, default=None,
    help="Password for basic auth.")
@click.option('-b', '--begin', type=str, required=False, default=None,
    help="ID to begin with. Skip importing all previous IDs.")
@click.argument('json_file', type=click.File('rb'))
def single(**kwargs):
    i = importer.Importer(
            endpoint=kwargs['endpoint'],
            auth=(kwargs['username'], kwargs['password'])
            )
    skip = bool(kwargs["begin"])
    for entry in processor.parse(kwargs['json_file']):
        if skip:
            if entry[0] == kwargs["begin"]:
                logger.info(f'found ID {entry[0]} to begin with')
                skip = False
            else:
                logger.debug(f'skipping ID {entry[0]}')
                continue
        i.import_entry(entry)

if __name__ == '__main__':
    cli()

